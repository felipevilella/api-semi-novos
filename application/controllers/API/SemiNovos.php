<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class SemiNovos extends REST_Controller  {

 	function __construct() {
	    parent::__construct();
		$this->load->library('form_validation');
	    $this->load->helper('feiraoAutomovel/semi_novos_helper');
	 }

 	/**
	* @author Felipe Viellla
	* Responsavel por obter dados dos veiculos disponibilizado no site seminovos.com.br
	**/

 	public function buscarVeiculos_get() {
 		$key = $this->input->get("key");

 		if ($key != 'b79e18c9bbf03530d70d76ee3e55e54f') {
 			return $this->response(array('tipo' => 'alert', 'mensagem' => 'Autenticação invalida!'), REST_Controller::HTTP_BAD_REQUEST);
 		}
	
 		try {
 			# obter dados
	 		$dados = array(
	 			'codigoMarca' => $this->input->get('marca'),
	 			'anoInicial' => $this->input->get('anoInicial'),
	 			'anoFinal' => $this->input->get('anoFinal'),
	 			'precoMinimo' => $this->input->get('precoMinimo'),
	 			'precoMaximo' => $this->input->get('precoMaximo'),
	 			'tiposVeiculos' => $this->input->get('tiposVeiculos')
	 		);

 			$veiculos = buscarVeiculo($dados);
 			return $this->response($veiculos, REST_Controller::HTTP_OK);

 		} catch (Exception $e) {
 			return $this->response(array('tipo'=> 'alert', 'mensagem' => $e->getMessage()), REST_Controller::HTTP_OK);
 		}
 	}
 }