<?php

	function buscarVeiculo($dadosPesquisa) {
		$url = "https://seminovos.com.br/carro";

		# Marca Veiculo
		if ($dadosPesquisa['codigoMarca']) {
			$marca = filtrarMarca($dadosPesquisa['codigoMarca']);
			
			if(!$marca) {
				throw new Exception("Marca de veiculo não encontrada");
			}
			$url = $url."/$marca";
		}

		# Ano inicial
		if ($dadosPesquisa['anoInicial']) {
			if ($dadosPesquisa['anoInicial'] < 1930) {
				throw new Exception("O ano inicial está invalido!");
			}
			
			$url = $url."/ano-.".$dadosPesquisa['anoInicial']."-";
		}

		# Ano Final
		if ($dadosPesquisa['anoFinal']) {	
			if ($dadosPesquisa['anoFinal'] > date('Y', strtotime('+12 months', strtotime(date('Y'))))) {
				throw new Exception("O ano final está invalido!");
			}
			
			if ($dadosPesquisa['anoInicial']) {
				$url = $url."/ano-.".$dadosPesquisa['anoInicial']."-".$dadosPesquisa['anoFinal'];
			} else {
				$url = $url."/ano--".$dadosPesquisa['anoFinal'];
			}
		}

		# Preço Minimo 
		if ($dadosPesquisa['precoMinimo']) {
			if(!in_array($dadosPesquisa['precoMinimo'], array('2000','4000','6000','8000','10000','12000','14000','16000','18000',
				'20000','22000','24000','26000','28000','30000','35000','40000','45000','50000','60000','70000','80000',
				'90000','100000','120000','140000','160000','180000','200000','250000','300000','350000','400000'))) {

				throw new Exception("O preço minimo está invalido!");
			}
	
			$url = $url."/ano-.".$dadosPesquisa['precoMinimo']."-";
		}

		# Preço Maximo
		if ($dadosPesquisa['precoMaximo']) {		
			if (!in_array($dadosPesquisa['precoMaximo'], array("2000","4000","6000","8000","10000","12000","14000",
				"16000","18000","20000","22000","24000","26000","28000","30000","35000","40000","45000","50000","60000",
				"70000","80000","90000","100000","150000","200000","250000","300000","350000","400000","450000","500000",
				"600000","700000","800000","900000","1000000","2000000"))) {

				throw new Exception("O preço Maximo está invalido!");
			}
			
			if ($dadosPesquisa['precoMinimo']) {
				$url = $url."/preco-.".$dadosPesquisa['precoMinimo']."-".$dadosPesquisa['precoMinimo'];
			} else {
				$url = $url."/preco--".$dadosPesquisa['precoMinimo'];
			}
		}

		# Verifica se os tipos de veiculos são separados por virgula ou ponto e virgula
		if($dadosPesquisa['tiposVeiculos']) {
			if (preg_match("/,/", $dadosPesquisa["tiposVeiculos"])) {
				$tiposVeiculos = explode(",", $dadosPesquisa["tiposVeiculos"]);
			} else if(preg_match("/;/", $dadosPesquisa["tiposVeiculos"])) {
				$tiposVeiculos = explode(";", $dadosPesquisa["tiposVeiculos"]);
			} else {
				$tiposVeiculos = array($dadosPesquisa["tiposVeiculos"]);
			} 

			$urlFiltro = filtrarTipVeiculo($tiposVeiculos);
			$url = $url."$urlFiltro";
		}
		
		# Retorno Final
		$url = file_get_contents($url);
		$dadosVeiculos = tratarDadosVeiculos($url);
		return array('tipo' => 'success', 'veiculos' => $dadosVeiculos);

	
	}

	function tratarDadosVeiculos($dados) {
		$nomes = array();
		$valores = array();
		$versoes = array();

		# Obter nomes dos veiculos
		preg_match_all('/<h2 class="card-title">(.*)</', $dados, $nomesVeiculos);
		foreach ($nomesVeiculos[0] as $nomeVeiculo) {
			preg_match_all('/>(.*)</', $nomeVeiculo, $nome);
			array_push($nomes, $nome[1][0]);
		}

		# Obter valor dos veiculos
		preg_match_all('/<span class="card-price(.*)</', $dados, $valoresVeiculos);
		foreach ($valoresVeiculos[0] as $valorVeiculo) {
			preg_match_all('/>(.*)</', $valorVeiculo, $valor);
			$valor = explode("</", $valor[1][0]);
			array_push($valores, $valor[0]);
		}
		
		# Obter versões dos veiculos
		preg_match_all('/<span>VERSÃO:(.*)</', $dados, $versoesVeiculos);
		foreach ($versoesVeiculos[1] as $versaoVeiculos) {
			array_push($versoes, $versaoVeiculos);
		}

		#Juntar os dados
		$lista = array();

		foreach ($nomes as $key => $nome) {
			$lista[$key]['nome'] = $nome;
			$lista[$key]['valor'] = $valores[$key];
			$lista[$key]['versao'] = $versoes[$key];
		}

		return $lista;
	}

	function filtrarTipVeiculo($tiposVeiculos) {
		$url = "";

		foreach ($tiposVeiculos as $tipoVeiculo) {
			switch ($tipoVeiculo) {
				case '1':
					$url = $url."/particular-origem";
					break;
				case '2':
					$url = $url."/revenda-origem";
					break;
				case '3':
					$url = $url."/novo-estado";
					break;
				case '4':
					$url = $url."/seminovo-estado";
					break;
				default:
					throw new Exception("O codigo tipo veiculo está invalido!");
				break;
			}
		}

		return $url;
	}

	function filtrarMarca($codigoVeiculo) {
		switch ($codigoVeiculo) {
			case 1:
				return 'Agrale';
				break;
			case 2:
				return 'Alfa Romeo';
				break;
			case 3:
				return 'Asia Motors';
				break;
			case 4:
				return 'Audi';
				break;
			case 5:
				return 'BMW';
				break;
			case 6:
				return 'Buggy';
				break;
			case 7:
				return 'Chevrolet';
				break;
			case 8:
				return 'Chrysler';
				break;
			case 9:
				return 'Citroen';
				break;
			case 10:
				return 'Comil';
				break;
			case 11:
				return 'Daewoo';
				break;
			case 12:
				return 'Daihatsu';
				break;
			case 13:
				return 'DKW';
				break;
			case 14:
				return 'Dodge';
				break;
			case 15:
				return 'Effa';
				break;
			case 16:
				return 'Engesa';
				break;
			case 17:
				return 'Ferrari';
				break;
			case 18:
				return 'Fiat';
				break;
			case 18:
				return 'Fiat';
				break;
			case 19:
				return 'Ford';
				break;
			case 19:
				return 'Ford';
				break;
			case 20:
				return 'Gurgel';
				break;
			case 21:
				return 'Hafei';
				break;
			case 22:
				return 'Honda';
				break;
			case 22:
				return 'Honda';
				break;
			case 23:
				return 'Hyundai';
				break;
			case 23:
				return 'Hyundai';
				break;
			case 24:
				return 'Isuzu';
				break;
			case 25:
				return 'Iveco';
				break;
			case 26:
				return 'Jaguar';
				break;
			case 27:
				return 'Jeep';
				break;
			case 27:
				return 'Jeep';
				break;
			case 28:
				return 'JPX';
				break;
			case 29:
				return 'Kia Motors';
				break;
			case 30:
				return 'Lada';
				break;
			case 31:
				return 'Land Rover';
				break;
			case 33:
				return 'Mascarelo';
				break;
			case 34:
				return 'Maserati';
				break;
			case 35:
				return 'Mazda';
				break;
			case 36:
				return 'Mercedes-Benz';
				break;
			case 37:
				return 'Mini';
				break;
			case 38:
				return 'Mitsubishi';
				break;
			case 38:
				return 'Mitsubishi';
				break;
			case 39:
				return 'Miura';
				break;
			case 40:
				return 'MP Lafer';
				break;
			case 41:
				return 'NeoBus';
				break;
			case 42:
				return 'Nissan';
				break;
			case 42:
				return 'Nissan';
				break;
			case 43:
				return 'Oldsmobile';
				break;
			case 44:
				return 'Mclaren';
				break;
			case 45:
				return 'Peugeot';
				break;
			case 45:
				return 'Peugeot';
				break;
			case 46:
				return 'Pontiac';
				break;
			case 47:
				return 'Porsche';
				break;
			case 48:
				return 'Puma';
				break;
			case 49:
				return 'Renault';
				break;
			case 49:
				return 'Renault';
				break;
			case 50:
				return 'Seat';
				break;
			case 51:
				return 'Ssangyong';
				break;
			case 52:
				return 'Subaru';
				break;
			case 53:
				return 'Suzuki';
				break;
			case 54:
				return 'Tanger';
				break;
			case 55:
				return 'Toyota';
				break;
			case 55:
				return 'Toyota';
				break;
			case 56:
				return 'Troller';
				break;
			case 57:
				return 'Volkswagen';
				break;
			case 57:
				return 'Volkswagen';
				break;
			case 58:
				return 'Volvo';
				break;
			case 59:
				return 'Walk';
				break;
			case 17:
				return 'Opel';
				break;
			case 19:
				return 'Chery';
				break;
			case 20:
				return 'Changan';
				break;
			case 23:
				return 'Smart';
				break;
			case 27:
				return 'Jac Motors';
				break;
			case 30:
				return 'Lifan Motors';
				break;
			case 32:
				return 'Emis';
				break;
			case 36:
				return 'Willys';
				break;
			case 37:
				return 'Mahindra';
				break;
			case 38:
				return 'Santa-Matilde';
				break;
			case 39:
				return 'Autocross';
				break;
			case 43:
				return 'Tac';
				break;
			case 44:
				return 'Lexus';
				break;
			case 45:
				return 'Mini-Veiculos';
				break;
			case 47:
				return 'Farus';
				break;
			case 49:
				return 'Lincoln';
				break;
			case 50:
				return 'Chana';
				break;
			case 55:
				return 'Hummer';
				break;
			case 56:
				return 'Cadillac';
				break;
			case 59:
				return 'Lotus';
				break;
			case 60:
				return 'Lamborghini';
				break;
			case 61:
				return 'Shineray';
				break;
			case 62:
				return 'Jinbei';
				break;
			case 65:
				return 'Pretty';
				break;
			case 68:
				return 'MG';
				break;
			case 70:
				return 'Infiniti';
				break;
			case 72:
				return 'Rolls-Royce';
				break;
			case 75:
				return 'Aston-Martin';
				break;
			case 76:
				return 'GMC';
				break;
			case 78:
				return 'Gonow';
				break;
			case 80:
				return 'Geely';
				break;
			case 81:
				return 'Triumph';
				break;
			case 84:
				return 'Americar';
				break;
			default:
				return false;
				break;
			}
		}


