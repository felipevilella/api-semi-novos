API site SEMINOVOS.COM.BR
# DESCRIÇÃO
Está API é responsável por trazer o nome, valor e a versão dos veículos de acordo com as pesquisas realizadas.
Para conhecer o código a API está localizada em application\controllers\API\SemiNovos.php, a estrutura que comunica com o site está em application\helpers\feiraoAutomovel\semi_novos_helper.php

# API
Url: localhost/feiraoAutomoveis/API/SemiNovos/buscarVeiculos <\n>
Tipo: GET  <\n>
Campos: <\n>
Key : b79e18c9bbf03530d70d76ee3e55e54f <\n>
codigoMarca: int <\n>
ano Inicial: int <\n>
ano Final: int <\n>
precoMinimo: int  <\n>
precoMaximo:int  <\n>
tiposVeiculos: int, pode inserir vários tipos de veículos separados por virgula <\n>

# Descrição dos campos:

# PREÇO MÍNIMO - OS VALORES ACEITO SÃO: 
'2000','4000','6000','8000','10000','12000','14000','16000','18000','20000','22000','24000','26000','28000','30000','35000','40000','45000','50000','60000','70000','80000','90000','100000','120000','140000','160000','180000','200000','250000','300000','350000','400000';

# PREÇO MÁXIMO - OS VALORES ACEITO SÃO:
"2000","4000","6000","8000","10000","12000","14000","16000","18000","20000","22000","24000","26000","28000","30000","35000","40000","45000","50000","60000","70000","80000","90000","100000","150000","200000","250000","300000","350000","400000","450000","500000","600000","700000","800000","900000","1000000","2000000";

# TIPOS DE VEÍCULOS:
1 - Particular <\n>
2 - Revenda <\n>
3 - Novo <\n>
4 - Seminovo <\n>
		


# CÓDIGO DE MARCA:
1-Agrale;
2-Alfa Romeo;
3-Asia Motors;
4-Audi;
5-BMW;
6-Buggy;
7-Chevrolet;
8-Chrysler;
9-Citroen;
10-Comil;
11-Daewoo;
12-Daihatsu;
13-DKW;
14-Dodge;
15-Effa;
16-Engesa;
17-Ferrari;
18-Fiat;
18-Fiat;
19-Ford;
19-Ford;
20-Gurgel;
21-Hafei;
22-Honda;
22-Honda;
23-Hyundai;
23-Hyundai;
24-Isuzu;
25-Iveco;
26-Jaguar;
27-Jeep;
27-Jeep;
28-JPX;
29-Kia Motors;
30-Lada;
31-Land Rover;
33-Mascarelo;
34-Maserati;
35-Mazda;
36-Mercedes-Benz;
37-Mini;
38-Mitsubishi;
38-Mitsubishi;
39-Miura;
40-MP Lafer;
41-NeoBus;
42-Nissan;
42-Nissan;
43-Oldsmobile;
44-Mclaren;
45-Peugeot;
45-Peugeot;
46-Pontiac;
47-Porsche;
48-Puma;
49-Renault;
49-Renault;
50-Seat;
51-Ssangyong;
52-Subaru;
53-Suzuki;
54-Tanger;
55-Toyota;
55-Toyota;
56-Troller;
57-Volkswagen;
57-Volkswagen;
58-Volvo;
59-Walk;
17-Opel;
19-Chery;
20-Changan;
23-Smart;
27-Jac Motors;
30-Lifan Motors;
32-Emis;
36-Willys;
37-Mahindra;
38-Santa-Matilde;
39-Autocross;
43-Tac;
44-Lexus;
45-Mini-Veiculos;
47-Farus;
49-Lincoln;
50-Chana;
55-Hummer;
56-Cadillac;
59-Lotus;
60-Lamborghini;
61-Shineray;
62-Jinbei;
65-Pretty;
68-MG;
70-Infiniti;
72-Rolls-Royce;
75-Aston-Martin;
76-GMC;
78-Gonow;
80-Geely;
81-Triumph;
84-Americar;